from flask import Flask, render_template, request

app = Flask(__name__)
@app.route("/<name>")
def func(name):
	if name.startswith("~") or name.startswith(".."):
		return app.send_static_file("403.html")
	try:
		file = open('static/'+name, 'r')
		response = file.read()
		file.close()
	except:
		return app.send_static_file("404.html")

	return app.send_static_file(f"{name}")

@app.route("/")
def func2():
	name = request.args.get("name")
	if name != None:
		return func(name)
	return f"Name is {name}"

# @app.errorhandler(403)
# def error403(403):
# 	return app.send_static_file("403.html")

# @app.errorhandler(404)
# def error403(404):
# 	return app.send_static_file("404.html")


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
